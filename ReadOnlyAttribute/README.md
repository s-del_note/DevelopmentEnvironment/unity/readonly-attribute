# ReadOnlyAttribute
インスペクタには表示されるが、非活性で編集不可なフィールドにするための属性。

## 使用方法
1. `using` にて `CustomAttribute.ReadOnlyAttribute` 名前空間を参照する
1. 編集不可にしたいフィールドに `[ReadOnly]` 属性を付与する
### サンプルコード
![SampleCode](/uploads/3b662b07d63bcd370b1a5ad3df39bec0/image.png)
### インスペクタ
![SampleInspector](/uploads/24b1accadc1becb844070392e32f49ab/image.png)
