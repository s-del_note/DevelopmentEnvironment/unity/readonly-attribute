using UnityEngine;

namespace CustomAttribute.ReadOnlyAttribute {
    public sealed class ReadOnly : PropertyAttribute {}
}
