using UnityEngine;
using UnityEditor;

namespace CustomAttribute.ReadOnlyAttribute.Editor {
    [CustomPropertyDrawer(typeof(ReadOnly))]
    internal sealed class ReadOnlyDrawer : PropertyDrawer {
        public override void OnGUI(
            Rect position, SerializedProperty property, GUIContent label
        ) {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUI.PropertyField(position, property, label);
            EditorGUI.EndDisabledGroup();
        }
    }
}
